Zombiedayz A19-4 ist ein Projekt von DeftChimp und Sechsterversuch
Modding:Sechsterversuch,Lichti,Renevandoll

Andere Mods sind eigentum, die ich hier liste und bleiben bei denen.
Wir haben für die Mods eine Zustimmung diese zu veröffentlichen. Wir verdienen damit kein Geld.
Die Abfrage das wir die Mods nutzen dürfen wurde durch eine Bestätigung im Chat/Mail zugestimmt.
Oder benötigen keine schriftliche anfragen sondern steht bei den in den Nutzungsbedingung.

Lizenz und Nutzungsbedingungen

1) Es steht Ihnen frei, diese Dateien herunterzuladen und im Einzelspieler zu spielen. Sie können diese Dateien auch auf einen Server hochladen,
   um Online zu spielen, vorausgesetzt, Sie besitzen eine legale Kopie des Spiels (7 Days To Die))

2) Es ist Ihnen NICHT gestattet, einen Code zu ändern, Modelle umzubenennen oder weiterzugeben.

3) Es steht Ihnen frei, ihre Erfahrung durch Änderung der XML-Dateien an ihre Bedürfnisse anzupassen. Wenn Sie einen Server oder ein Spiel hosten
   mit solchen veränderten Inhalten, ist es Ihnen NICHT gestattet, diese Änderungen ohne meine ausdrückliche Genehmigung weiterzugeben.
   Sie MÜSSEN die Erlaubnis von mir einholen, bevor Dateien, Code, Modelle oder Assets, die in meinen Mods enthalten sind
   die nicht auf Ihrem PC oder auf einem dedizierten Server für den privaten Gebrauch freigegeben werden.

4) Benutzern ist es NICHT erlaubt, den Code aus dieser Mod zu nehmen, um ihn in irgendeiner Weise zu einer anderen Mod hinzuzufügen,
   ohne mich vorher zu fragen. Dies gilt auch für die Entnahme (von Teilen) des Codes, von XML oder Assets sowie von geänderten Versionen davon.
   
5) Diese Versionen des Mod ist speziell für das modPack (den Overhaul) ZombieDayz von 𝕾𝖊𝖈𝖍𝖘𝖙𝖊𝖗𝖛𝖊𝖗𝖘𝖚𝖈𝖍 (Discord: https://discord.gg/X72gTTV).
   Sie darf nur als Bestandteil dieses modPack/Overhaul weitergegeben werden. Als Bestandteil dieses modPack/Overhaul darf es
   unbegrenzt verändert und angepasst werden, solange die folgenden aufgelisteteten Dateien unverändert bleiben:
		- EN_License_Information.md
		- EN_Readme.md
		- GER_License_Information.md
		- GER_Readme.md
		- mod.xml
		- ModInfo.xml
		- Contact_info.md

Alle Rechte vorbehalten, 2021 zombiedayz Webseite: https://www.zombiedayz.info/


Ein Großen Dank geht an den Moddern deren wir die Mods nutzen dürfen die nicht unsere eigene sind.

ShoudenKalferas
Bdubyah
JaxTeller718
Guppycur
Gyancher2
Khain
Sirillon
Stallionsden (Valmar)
Stasis
Snufkin
Telrics
Wolfbain 5
Xyth
Riles
bdubyah
Polyman
Lichti
Ragsy
Renevandoll